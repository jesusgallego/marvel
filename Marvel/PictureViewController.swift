//
//  PictureViewController.swift
//  Marvel
//
//  Created by Master Móviles on 17/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit
import Marvelous

class PictureViewController: UIViewController {
    
    var picture : RCImageObject?

    @IBOutlet weak var imagen: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let colaBackground = OperationQueue()
        colaBackground.addOperation {
            if let thumb = self.picture {
                let url = "\(thumb.basePath!)/portrait_xlarge.\(thumb.extension!)"
                let urlHttps = url.replacingOccurrences(of: "http", with: "https")
                if let urlFinal = URL(string:urlHttps) {
                    do {
                        let datos = try Data(contentsOf:urlFinal)
                        if let img = UIImage(data: datos) {
                            OperationQueue.main.addOperation {
                                self.imagen.image = img
                            }
                        }
                    }
                    catch {
                    }
                }
            }
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
