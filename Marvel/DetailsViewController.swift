//
//  DetailsViewController.swift
//  Marvel
//
//  Created by Master Móviles on 17/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit
import Marvelous

class DetailsViewController: UIViewController {
    
    var detailsCharacter: RCCharacterObject?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var imagen: UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = detailsCharacter?.name
        descriptionLabel.text = detailsCharacter?.bio
        
        
        let colaBackground = OperationQueue()
        colaBackground.addOperation {
            if let thumb = self.detailsCharacter?.thumbnail {
                let url = "\(thumb.basePath!)/portrait_uncanny.\(thumb.extension!)"
                let urlHttps = url.replacingOccurrences(of: "http", with: "https")
                if let urlFinal = URL(string:urlHttps) {
                    do {
                        let datos = try Data(contentsOf:urlFinal)
                        if let img = UIImage(data: datos) {
                            OperationQueue.main.addOperation {
                                self.imagen.image = img
                            }
                        }
                    }
                    catch {
                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ampliarImagen(_ sender: AnyObject) {
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPicture" {
            let controller = segue.destination as! PictureViewController
            controller.picture = self.detailsCharacter?.thumbnail
        }
    }
 
    
    @IBAction func volver(segue: UIStoryboardSegue) {
        print("Back")
    }

}
