//
//  MarvelAPI.swift
//  Marvel
//
//  Created by Master Móviles on 17/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import Foundation
import Marvelous

class MarvelAPI {
    
    var marvelous : RCMarvelAPI
    
    private let publicKey = "91d897d7407f9c4427f0b3707f670e84"
    private let privateKey = "ad050c97e07c2145b785926715988e67e62a6ac6"
    
    init() {
        
        marvelous = RCMarvelAPI()
        
        marvelous.publicKey = "91d897d7407f9c4427f0b3707f670e84"
        marvelous.privateKey = "ad050c97e07c2145b785926715988e67e62a6ac6"
    }
    
    func getCharacters(startingWith name: String, completion: @escaping ([RCCharacterObject]) -> Void) {
        let filtro = RCCharacterFilter()
        filtro.nameStartsWith = name
        marvelous.characters(by: filtro) {
            res, info, err in
            if let characters = res as! [RCCharacterObject]? {
                completion(characters)
            }
        }
    }
    
}
